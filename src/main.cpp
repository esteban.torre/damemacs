#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "sdk_structs.hpp"

void mac2str(const uint8 *ptr, char* string)
{
  sprintf(string, "%02x:%02x:%02x:%02x:%02x:%02x", ptr[0], ptr[1], ptr[2], ptr[3], ptr[4], ptr[5]);
}

typedef struct {
  uint8_t addr[6]; // keep 1st or refactor mac_list_find & others
} target_bssid_t;

#define MAX_TARGET_BSSID 100
target_bssid_t target_bssids[MAX_TARGET_BSSID]; /* 6*100 bytes */
target_bssid_t zero_target = {
  .addr = {0,0,0,0,0,0}
};
target_bssid_t broadcast_target = {
  .addr = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}
};
int old_target = -1;

typedef struct{
  uint8_t addr[6]; // keep 1st or refactor mac_list_find & others
} reward_mac_t;
#define MAX_REWARD_MAC 1000 
reward_mac_t reward_macs[MAX_REWARD_MAC]; /*6 * 1000 bytes */
int old_reward = -1;

const char target_marker_ssid[] = "TeleCentro Wifi";

bool mac_is_multicast(uint8_t* mac) {
    // see https://en.wikipedia.org/wiki/Multicast_address
    if ((mac[0] == 0x33) && (mac[1] == 0x33)) return true;

    if ((mac[0] == 0x01) && (mac[1] == 0x80) && (mac[2] == 0xC2)) return true;

    if ((mac[0] == 0x01) && (mac[1] == 0x00) && ((mac[2] == 0x5E) || (mac[2] == 0x0C))) return true;

    if ((mac[0] == 0x01) && (mac[1] == 0x0C) && (mac[2] == 0xCD) &&
        ((mac[3] == 0x01) || (mac[3] == 0x02) || (mac[3] == 0x04)) &&
        ((mac[4] == 0x00) || (mac[4] == 0x01))) return true;

    if ((mac[0] == 0x01) && (mac[1] == 0x00) && (mac[2] == 0x0C) && (mac[3] == 0xCC) && (mac[4] == 0xCC) &&
        ((mac[5] == 0xCC) || (mac[5] == 0xCD))) return true;

    if ((mac[0] == 0x01) && (mac[1] == 0x1B) && (mac[2] == 0x19) && (mac[3] == 0x00) && (mac[4] == 0x00) &&
        (mac[5] == 0x00)) return true;

    return false;
}

int mac_list_find(const uint8_t* list, size_t increment, size_t max_items, const uint8_t* target)
{
  /* FOUND: returns postion (>= 0)
   * NOT FOUND (end of list): returns -max_items
   * NOT FOUND (found empty entry): returns -empty_index
   */
  for(size_t i = 0; i < max_items; ++i)
  {
    if(memcmp(zero_target.addr, &list[increment*i], 6) == 0)
      return -i;
    if(memcmp(target, &list[increment*i], 6) == 0)
      return i;
  }
  return -max_items;
}

int target_list_find(const target_bssid_t* target)
{
  return mac_list_find((uint8_t*)target_bssids, sizeof(target_bssid_t), MAX_TARGET_BSSID,
                      target->addr);
}

int reward_list_find(const reward_mac_t* reward)
{
  return mac_list_find((uint8_t*)reward_macs, sizeof(reward_mac_t), MAX_REWARD_MAC,
                        reward->addr);
}

void acquire_target(const target_bssid_t* target)
{
  int n = 0;
  if(old_target == -1)
    old_target = 0;
  else
  {
    n = target_list_find(target);
    if(n >= 0) /* FOUND */
      return;
    if(n == -MAX_TARGET_BSSID) /* NOT FOUND & LIST FULL */
    {
      n = old_target;
      old_target++;
      if(old_target == MAX_TARGET_BSSID)
        old_target = 0;
    }else /* NOT FOUND & -N POSITION IN LIST IS EMPTY */
      n = -n;
  }
  
  memcpy(target_bssids[n].addr, target->addr, 6); 
  char addr1[] = "00:00:00:00:00:00\0";
  mac2str(target->addr, addr1);
  Serial.println("BSSID:");
  Serial.println(addr1);
}

int beacon_is_marked(const wifi_mgmt_beacon_t *beacon_frame)
{
  size_t ln = sizeof(target_marker_ssid)-1;
  if(beacon_frame->tag_length == ln &&
    strncmp(target_marker_ssid, beacon_frame->ssid, ln) == 0)
    return 1;
  return 0;
}

void scan_beacon(const wifi_mgmt_beacon_t *beacon_frame, const uint8_t* addr)
{
  if(beacon_is_marked(beacon_frame))
    acquire_target((target_bssid_t*)addr);
}

void print_mac(uint8_t* mac)
{
  char addr1[] = "00:00:00:00:00:00\0";
  mac2str(mac, addr1);
  Serial.println(addr1);
}
void acquire_reward(target_bssid_t* bssid, reward_mac_t* addr)
{
  if(old_target == -1 || memcmp(bssid, addr, 6) == 0 ||
    memcmp(bssid,broadcast_target.addr,6) == 0 ||
    memcmp(addr,broadcast_target.addr,6) == 0 ||
    mac_is_multicast(bssid->addr) ||
    mac_is_multicast(addr->addr))
    return;
  int n = target_list_find(bssid);
  if(n>=0)
  {
    int m = 0;
    if(old_reward == -1)
      old_reward = 0;
    else
    {
      m = reward_list_find(addr);
      if(m >= 0) /* FOUND */
        return;

      if(m == -MAX_REWARD_MAC) /* NOT FOUND & LIST FULL */
      {
        m = old_reward;
        old_reward++;
        if(old_reward == MAX_REWARD_MAC)
          old_reward = 0;
      }else /* NOT FOUND & -M POSITION IN LIST IS EMPTY */
        m = -m;
    }

    Serial.printf("Saving on record %d\n", m);
    memcpy(reward_macs[m].addr, addr->addr, 6); 
    Serial.println("Cliente:");
    print_mac(reward_macs[m].addr);

  }

}

void sniffer(uint8_t *buf, uint16_t len) {
  if(len < 28)
    return; /* Drop invalid packets */
  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buf;
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;
  //const uint8_t *data = ipkt->payload;

  // Pointer to the frame control section within the packet header
  const wifi_header_frame_control_t *frame_ctrl = (wifi_header_frame_control_t *)&hdr->frame_ctrl;

  if (frame_ctrl->type == WIFI_PKT_MGMT && frame_ctrl->subtype == BEACON)
  {
    const wifi_mgmt_beacon_t *beacon_frame = (wifi_mgmt_beacon_t*) ipkt->payload;
    scan_beacon(beacon_frame, hdr->addr3);
  }
  else if(frame_ctrl->type == WIFI_PKT_DATA)
  {
    acquire_reward((target_bssid_t*)hdr->addr1, (reward_mac_t*) hdr->addr2);
    acquire_reward((target_bssid_t*)hdr->addr2, (reward_mac_t*) hdr->addr1);
  }
}

void setup() {
  Serial.begin(57600);
  Serial.println("Holis");
  // put your setup code here, to run once:
  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(0);
  WiFi.disconnect();
  wifi_set_promiscuous_rx_cb(sniffer);
  wifi_set_channel(11);
  wifi_promiscuous_enable(1);
  Serial.println("Escuchando...");
}

int channel = 11;
void loop() {

  // put your main code here, to run repeatedly:
}